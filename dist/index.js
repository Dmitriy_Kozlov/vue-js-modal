(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"));
	else if(typeof define === 'function' && define.amd)
		define(["vue"], factory);
	else if(typeof exports === 'object')
		exports["vue-js-modal"] = factory(require("vue"));
	else
		root["vue-js-modal"] = factory(root["vue"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(1);

var _Modal2 = _interopRequireDefault(_Modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultComponentName = 'modal';

var Plugin = {
  install: function install(Vue) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    if (this.installed) {
      return;
    }

    this.installed = true;
    this.event = new Vue();

    Vue.prototype.$modal = {
      show: function show(name, params) {
        Plugin.event.$emit('toggle', name, true, params);
      },
      hide: function hide(name, params) {
        Plugin.event.$emit('toggle', name, false, params);
      }
    };

    var componentName = options.componentName || defaultComponentName;
    Vue.component(componentName, _Modal2.default);
  }
};

exports.default = Plugin;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(3)(
  /* script */
  __webpack_require__(2),
  /* template */
  __webpack_require__(4),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/cult/Projects/vue-js-modal/src/Modal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-40dd3b1e", Component.options)
  } else {
    hotAPI.reload("data-v-40dd3b1e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _vue = __webpack_require__(5);

var _vue2 = _interopRequireDefault(_vue);

var _index = __webpack_require__(0);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'VueJsModal',
    props: {
        name: {
            required: true,
            type: String
        },
        scrollable: {
            type: Boolean,
            default: false
        },
        transition: {
            type: String
        },
        clickToClose: {
            type: Boolean,
            default: true
        },
        classes: {
            type: [String, Array],
            default: 'v--modal'
        }
    },
    data: function data() {
        return {
            visible: false,

            visibility: {
                modal: false,
                overlay: false
            }

        };
    },

    watch: {
        visible: function visible(value) {
            var _this = this;

            if (value) {
                this.visibility.overlay = true;

                setTimeout(function () {
                    document.body.classList.add('overflow');
                    _this.visibility.modal = true;
                }, 0);
            } else {
                this.visibility.modal = false;

                setTimeout(function () {
                    document.body.classList.remove('overflow');
                    _this.visibility.overlay = false;
                }, 0);
            }
        }
    },
    beforeMount: function beforeMount() {
        var _this2 = this;

        _index2.default.event.$on('toggle', function (name, state, params) {
            if (name === _this2.name) {
                if (typeof state === 'undefined') {
                    state = !_this2.visible;
                }

                _this2.toggle(state, params);
            }
        });
    },

    computed: {
        overlayClass: function overlayClass() {
            return {
                'v--modal-overlay': true,
                'scrollable': this.scrollable
            };
        },
        modalClass: function modalClass() {
            return ['v--modal-box', this.classes];
        }
    },
    methods: {
        genEventObject: function genEventObject(params) {
            var data = {
                name: this.name,
                timestamp: Date.now(),
                canceled: false,
                ref: this.$refs.modal
            };

            return _vue2.default.util.extend(data, params || {});
        },
        toggle: function toggle(state, params) {
            var visible = this.visible;


            var beforeEventName = visible ? 'before-close' : 'before-open';

            var afterEventName = visible ? 'closed' : 'opened';

            var stopEventExecution = false;

            var stop = function stop() {
                stopEventExecution = true;
            };

            var beforeEvent = this.genEventObject({ stop: stop, state: state, params: params });

            this.$emit(beforeEventName, beforeEvent);

            if (!stopEventExecution) {
                var afterEvent = this.genEventObject({ state: state, params: params });
                this.visible = state;
                this.$emit(afterEventName, afterEvent);
            }
        },
        onBackgroundClick: function onBackgroundClick() {
            if (this.clickToClose) {
                this.toggle(false);
            }
        }
    }
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('transition', {
    attrs: {
      "name": "overlay-fade"
    }
  }, [(_vm.visibility.overlay) ? _c('div', {
    ref: "overlay",
    class: _vm.overlayClass,
    attrs: {
      "aria-expanded": _vm.visible.toString(),
      "data-modal": _vm.name
    },
    on: {
      "mousedown": function($event) {
        $event.stopPropagation();
        _vm.onBackgroundClick($event)
      },
      "touchstart": function($event) {
        $event.stopPropagation();
        _vm.onBackgroundClick($event)
      }
    }
  }, [_c('transition', {
    attrs: {
      "name": _vm.transition
    }
  }, [(_vm.visibility.modal) ? _c('div', {
    ref: "modal",
    class: _vm.modalClass,
    on: {
      "mousedown": function($event) {
        $event.stopPropagation();
      },
      "touchstart": function($event) {
        $event.stopPropagation();
      }
    }
  }, [_c('div', {
    staticClass: "v--modal-top-right",
    on: {
      "mousedown": function($event) {
        $event.stopPropagation();
        _vm.onBackgroundClick($event)
      },
      "touchstart": function($event) {
        $event.stopPropagation();
        _vm.onBackgroundClick($event)
      }
    }
  }, [_vm._t("top-right")], 2), _vm._v(" "), _vm._t("default")], 2) : _vm._e()])], 1) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-40dd3b1e", module.exports)
  }
}

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=index.js.map